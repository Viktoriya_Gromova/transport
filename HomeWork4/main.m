//
//  main.m
//  HomeWork4
//
//  Created by Chaban Nikolay on 12/2/15.
//  Copyright © 2015 ViktoriyaGromova. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    car,
    bicycle,
    bus,
    truck
}
TransportType;

typedef enum
{
    black,
    cherry,
    white,
    blue
}
TransportColor;

typedef struct
{
    TransportType type;
    TransportColor color;
}
Transport;

void createTransports();
int getRandomInRange(int lowerBound, int upperBound);

int main(int argc, const char * argv[])
{
    @autoreleasepool
    {
        createTransports();
        
    }
    return 0;
}

void createTransports()
{
    for(int i = 1; i <= 4; i++)
    {
        Transport transport;
        
        transport.type  = getRandomInRange(0, 3);
        transport.color = getRandomInRange(0, 3);
        
        // Main log for both modes which contain transport type
        NSLog(@"Type of the transport %i", transport.type);
        
#if DEBUG
        NSLog(@"The %i have a %i color", transport.type, transport.color);
#endif
        
    }
}

int getRandomInRange(int lowerBound, int upperBound)
{
    int randValue = lowerBound + arc4random_uniform(upperBound - lowerBound);
    
    return randValue;
}